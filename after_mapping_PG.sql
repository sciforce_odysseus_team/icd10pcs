--create table for all acceptable relations between a procedure and an attribute inside SNOMED
drop table if exists relations;
create table relations (relationship_id varchar (127))
;
insert into relations values ('During');
insert into relations values ('Has direct site');
insert into relations values ('Has technique');
insert into relations values ('Followed by');
insert into relations values ('Using subst');
insert into relations values ('Using device');
insert into relations values ('Using energy');
insert into relations values ('Using acc device');
insert into relations values ('Occurs after');
insert into relations values ('Has surgical appr');
insert into relations values ('Has scale type');
insert into relations values ('Has property');
insert into relations values ('Has proc site');
insert into relations values ('Has indir proc site');
insert into relations values ('Has dir proc site');
insert into relations values ('Has proc morph');
insert into relations values ('Has proc device');
insert into relations values ('Has proc context');
insert into relations values ('Has pathology');
insert into relations values ('Has asso morph');
insert into relations values ('Has method');
insert into relations values ('Has specimen');
insert into relations values ('Has dir subst');
insert into relations values ('Has dir morph');
insert into relations values ('Has dir device');
insert into relations values ('Has asso finding');
insert into relations values ('Has asso morph');
insert into relations values ('Has surgical appr');
insert into relations values ('Has access');
;
/*create unlogged table attr_from_usagi
	(
		attr_name varchar(255),
		concept_id int4,
		concept_name varchar(255),
		NEW_CODE varchar(255),
		NEW_NAME varchar(255),
		extra varchar(255)
	);*/
-- upload usagi/manually mapped table
WbImport -file=/home/ekorchmar/i10patos_fu
			-type=text
			-table=ATTR_FROM_USAGI
			-encoding="UTF-8"
			-header=true
			-decode=false
			-dateFormat="yyyy-MM-dd"
			-timestampFormat="yyyy-MM-dd HH:mm:ss"
			-delimiter='\t'
			-quotechar='"'
			-decimal=.
			-fileColumns=ATTR_NAME,CONCEPT_ID,CONCEPT_NAME,NEW_CODE,NEW_NAME,EXTRA
			-quoteCharEscaping=none
			-ignoreIdentityColumns=false
			-deleteTarget=true
			-continueOnError=false
			-batchSize=1000;
;
drop table if exists attr_map;
create unlogged table attr_map as
select
	attr_name,
	coalesce (c.concept_id, a.concept_id) as concept_id
from attr_from_usagi a
left join concept c on
	a.new_code = c.concept_code and
	c.vocabulary_id = 'SNOMED'
where extra is null
;
delete from attr_map where concept_id is null
;
drop table if exists icd10mappings;
--final *source* table we will work with
create unlogged table icd10mappings as
select
	c.concept_id as procedure_id,
	u.concept_code as procedure_code,
	c.concept_name as procedure_name,
	a.concept_id as attribute_id,
	c2.concept_name as attribute_name,
	c2.concept_class_id,
	u.priority
from atr_unil u
join concept c on
	--c.vocabulary_id = 'ICD10PCS' and
	c.concept_id = u.concept_id
join attr_map a on
	u.attr_name = a.attr_name
join concept c2 on
	c2.concept_id = a.concept_id
;
CREATE INDEX IDX_ICD10MAPPINGS ON ICD10MAPPINGS (ATTRIBUTE_ID, PROCEDURE_ID,CONCEPT_CLASS_ID)
;
analyze ICD10MAPPINGS
;
delete from icd10mappings where length (procedure_code) < 6
;
--comment whole indented section if no lvl6 manual mappings were made
		drop table if exists level6
		;
		create table level6 
			(
				ICD_CODE varchar (255),
				ICD_ID int4,
				ICD_NAME varchar (255),
				SNOMED_CODE varchar (31),
				SNOMED_NAME varchar (255),
				snomed_code_2 varchar (31),
				snomed_name_2 varchar (255)
			)
		;
		WbImport -file=/home/ekorchmar/Documents/newicd.csv
		         -type=text
		         -table=level6
		         -encoding="UTF-8"
		         -header=true
		         -decode=false
		         -dateFormat="yyyy-MM-dd"
		         -timestampFormat="yyyy-MM-dd HH:mm:ss"
		         -delimiter='\t'
		         -quotechar='"'
		         -decimal=.
		         -fileColumns=icd_code,icd_id,icd_name,snomed_code,snomed_name,snomed_code_2,snomed_name_2
		         -quoteCharEscaping=none
		         -ignoreIdentityColumns=false
		         -deleteTarget=true
		         -continueOnError=false
		         -batchSize=1000
		;
		update level6 n
		set ICD_CODE = 
			(
				select concept_code from concept c where c.concept_id = n.icd_id
			)
		;
		with full_list as
			(
				select distinct
					icd_id,
					icd_code,
					snomed_code
				from level6
				where snomed_code is not null
					UNION
				select distinct
					icd_id,
					icd_code,
					snomed_code_2
				from level6
				where snomed_code_2 is not null
			)
		insert into icd10mappings
		select
			ic.concept_id,
			ic.concept_code,
			ic.concept_name,
			c.concept_id,
			c.concept_name,
			c.concept_class_id,
			8 as priority
		from concept ic
		join full_list f on
			ic.vocabulary_id = 'ICD10PCS' and
			f.icd_code = substr (ic.concept_code,1,6)
		join concept c on
			c.vocabulary_id = 'SNOMED' and
			f.snomed_code = c.concept_code
		;
		with full_list as --insert attributes too
			(
				select distinct
					icd_id,
					icd_code,
					snomed_code
				from level6
				where snomed_code is not null
					UNION
				select distinct
					icd_id,
					icd_code,
					snomed_code_2
				from level6
				where snomed_code_2 is not null
			)
		insert into icd10mappings
		select
			ic.concept_id,
			ic.concept_code,
			ic.concept_name,
			ca.concept_id,
			ca.concept_name,
			ca.concept_class_id,
			18 as priority
		from full_list f
		join concept ic on
			ic.vocabulary_id = 'ICD10PCS' and
			f.icd_code = substr (ic.concept_code,1,6)
		join concept c on
			c.vocabulary_id = 'SNOMED' and
			f.snomed_code = c.concept_code
		join concept_relationship cr on
			c.concept_id = cr.concept_id_1 and
			cr.invalid_reason is null
		join relations r on
			r.relationship_id = cr.relationship_id
		join concept ca on
			cr.concept_id_2 = ca.concept_id
		;

drop table if exists false_pairs cascade
;
--remove parent attributes where their children are present, for clarity
create unlogged table false_pairs as
	select
		i.procedure_id, i.attribute_id
	from icd10mappings i
	join concept_ancestor a	on
		i.attribute_id = a.ancestor_concept_id and a.min_levels_of_separation != 0
	join icd10mappings i2 on
		i2.procedure_id = i.procedure_id and
		a.descendant_concept_id = i2.attribute_id
;
delete from icd10mappings 
where (procedure_id, attribute_id) in (select * from false_pairs);
;
/* REPLACED WITH SPLITTER SEQUENCE
--remove procedures with multiple procedure-attributes
--these can exist due to errors in manual attribute mappings (where something that is not a procedure made one)
--or because of SNOMED hierarchy shortcomings (both logical parent and child were present but not removed in previous step)
create unlogged table to_delete as
with x as
	(
		select distinct procedure_id from icd10mappings
		where concept_class_id = 'Procedure'
		group by procedure_id
		having count(distinct attribute_id)>1
	)
select distinct i.PROCEDURE_ID from icd10mappings i
join x on x.procedure_id = i.procedure_id
where concept_class_id = 'Procedure'
;
delete from icd10mappings
where procedure_id in
	(select * from to_delete)
;
drop table if exists to_delete cascade
;
*/
--split procedures with multiple attributes
;
drop table if exists splitter
;
drop sequence if exists nv1
;
CREATE SEQUENCE nv1 INCREMENT BY 1 START WITH 2000000000 NO CYCLE CACHE 20
;--create table that will contain original icd ids and new
create table splitter as
with x as
	(
		select distinct procedure_id from icd10mappings
		where concept_class_id = 'Procedure'
		group by procedure_id
		having count(distinct attribute_id)>1
	)
select
	nextval ('nv1') as replaced_id,
	i.procedure_id,
	i.attribute_id as conflict_id
from icd10mappings i
join x on
	x.procedure_id = i.procedure_id and
	i.concept_class_id = 'Procedure'
;--repopulate replacements back into icd10mappings
insert into icd10mappings
	select 
		s.replaced_id as procedure_id,
		i.procedure_code,
		i.procedure_name,
		i.attribute_id,
		i.attribute_name,
		i.concept_class_id,
		i.priority
from splitter s
join icd10mappings i on
	i.procedure_id = s.procedure_id and
		( --attribute is NOT a procedure OR exactly one of the procedures caused conflict
			i.concept_class_id != 'Procedure' or
			i.attribute_id = conflict_id
		)
;
delete from icd10mappings 
where procedure_id in
	(
		select procedure_id from splitter
	)
;
analyze ICD10MAPPINGS
;
-- RERUN FROM HERE
drop table if exists mappings cascade
;
create table mappings -- final resulting table, subject for manual checks
	(
		procedure_id int4,
		snomed_id int4,
		rel_id varchar (127),
		priority int4
	)
;
drop table if exists test_group cascade
-- this table will contain ALL possible ICD10 to SNOMED matches
-- exactly one match with procedure-attribute or it's descendant
-- at least one match on any other attribute or it's ancestor
;
create unlogged table test_group as

select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, lower number = more precise
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	x.concept_class_id, -- to store if attribute is a device; important to determine travelling direction in hierarchy
	x.priority -- number to store corresponding attribute letter position in ICD10PCS code for later filtering
from icd10mappings i
join devv5.concept_ancestor a on  --Include all procedure descendants
	i.concept_class_id = 'Procedure' and
	a.ancestor_concept_id = i.attribute_id
	--and substr (i.procedure_code,1,3) = '0R9' --uncomment to test on single code group, useful to check mappings
left join concept_relationship cx on --method HAS to be specified for this iteration
	cx.concept_id_1 = i.attribute_id and
	cx.invalid_reason is null and
	substr (i.procedure_code,1,1) in ('0') and --surgical procedures
	cx.relationship_id = 'Has method' and
	cx.concept_id_2 != 4301351 --surgical procedure (generic)
join icd10mappings x on --place horizontally procedures and all other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --include ancestors of all other attributes
	ra.descendant_concept_id = x.attribute_id and
	ra.max_levels_of_separation <=3 --limit set because of acceptability and RAM limits reasons
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_1 = a.descendant_concept_id and
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on
	 s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on
	 s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join sources.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
where cx.concept_id_1 is null
;

--separate insert -- 5th level exceeds memory limit
insert into test_group
select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, more precise is better
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better
	x.concept_class_id, -- to store if attribute is a body site; important to take in the account for topographic procedures
	x.priority
from icd10mappings i
join devv5.concept_ancestor a on
	i.concept_class_id = 'Procedure' and
	a.ancestor_concept_id = i.attribute_id
	--and substr (i.procedure_code,1,3) = '0R9' --uncomment to test on single code group, useful to check mappings
left join concept_relationship cx on --method HAS to be specified for this iteration
	cx.concept_id_1 = i.attribute_id and
	cx.invalid_reason is null and
	substr (i.procedure_code,1,1) in ('0') and --surgical procedures
	cx.relationship_id = 'Has method' and
	cx.concept_id_2 != 4301351 --surgical procedure (generic)
join icd10mappings x on --place horizontally procedures and other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --extend other attributes vertically up
	ra.descendant_concept_id = x.attribute_id and
	ra.max_levels_of_separation in (4,5) --limit set because of acceptability and RAM limits reasons
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_1 = a.descendant_concept_id and
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join relations e on --ensure validness of relationship
	e.relationship_id = r.relationship_id
join concept s on
	 s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on
	 s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join sources.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
where cx.concept_id_1 is null
;

--separate insert for devices: devices should be mapped down the hierarchy too.
insert into test_group
select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.descendant_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far down is the mapping of the device, lower number = more precise;
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	x.concept_class_id, -- to store if attribute is a body site; important to take in the account for topographic procedures
	x.priority -- to store other info for later filtering
from icd10mappings i
join devv5.concept_ancestor a on  --Include all procedure descendants
	i.concept_class_id = 'Procedure' and
	a.ancestor_concept_id = i.attribute_id
	--and substr (i.procedure_code,1,3) = '0R9' --uncomment to test on single code group, useful to check mappings
left join concept_relationship cx on --method HAS to be specified for this iteration
	cx.concept_id_1 = i.attribute_id and
	cx.invalid_reason is null and
	substr (i.procedure_code,1,1) in ('0') and --surgical procedures
	cx.relationship_id = 'Has method' and
	cx.concept_id_2 != 4301351 --surgical procedure (generic)
join icd10mappings x on --place horizontally procedures and devices
	x.procedure_id = i.procedure_id and
	x.concept_class_id = 'Device'--in ('Device', 'Qualifier Value')
join devv5.concept_ancestor ra on --include descendants of devices
	ra.ancestor_concept_id = x.attribute_id and
	ra.max_levels_of_separation in (1,2) --limit set because of acceptability and RAM limits reasons
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_1 = a.descendant_concept_id and
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on
	 s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on
	 s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join sources.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
where cx.concept_id_1 is null
;
CREATE INDEX idx_test_group	ON test_group (procedure_id, snomed_id) 
;
analyze test_group
;
--No snomed procedure should have Method more specific than root procedure in pos. 3
--Repairs and removals are exception as their interrelationships are inconsistent in snomed
drop table if exists false_method
;
create unlogged table false_method as
with repairs as --children of repair and removal method
	(
		select descendant_concept_id as repairs_id
		from concept_ancestor
		where ancestor_concept_id in (4117981,4044524)
	)
select
	t.procedure_id,
	t.snomed_id
from test_group t
join icd10mappings i on
	substr (i.procedure_code,1,1) in ('0') and --surgical procedures
	i.procedure_id = t.procedure_id and
	i.concept_class_id = 'Procedure'
join concept_relationship r1 on
	i.attribute_id = r1.concept_id_1 and
	r1.relationship_id = 'Has method' and
	r1.invalid_reason is null
join concept_relationship r2 on
	t.snomed_id = r2.concept_id_1 and
	r2.relationship_id = 'Has method' and
	r2.invalid_reason is null and
	r1.concept_id_2 != r2.concept_id_2 -- Method differs
left join repairs rr on
	r2.concept_id_2 = rr.repairs_id
where rr.repairs_id is null -- did not get into repairs cathegory
;
CREATE INDEX idx_false_method2 ON false_method (procedure_id,snomed_id) 
;
analyze false_method
;
drop table if exists test_group1 cascade
;
create unlogged table test_group1 as
select t.*
from test_group t
left join false_method f on
	f.procedure_id = t.procedure_id and
	f.snomed_id = t.snomed_id
where f.procedure_id is null
;
drop table if exists test_group cascade
;
drop table if exists false_method cascade
;
alter table test_group1 rename to test_group
;
CREATE INDEX idx_test_group	ON test_group (procedure_id, snomed_id, match_on)
;
analyze test_group
;/*
-- remove procedures with too many excessive attributes
create unlogged table sculptor as
with true_counts as
	(
		select --how many relations to attributes each SNOMED procedure has
			cr.concept_id_1 as snomed_id,
			count (cr.concept_id_2) as rcounter
		from concept_relationship cr
		join relations r on --limit to valid attribute relations
			r.relationship_id = cr.relationship_id
		join concept c on --search among snomed procedures
			c.vocabulary_id = 'SNOMED' and
			c.standard_concept = 'S' and
			c.concept_class_id = 'Procedure' and
			cr.concept_id_1 = c.concept_id
		join concept c2 on --only snomed attributes
			concept_id_2 = c2.concept_id and
			c2.vocabulary_id = 'SNOMED'
		group by cr.concept_id_1
	),
sculptor_unfiltered as
	(
		select distinct
			g.procedure_id,
			t.rcounter,
			t.snomed_id,
			count (g.match_on) over (partition by g.procedure_id, g.snomed_id) as matches
		from test_group g
		join true_counts t on
			t.snomed_id = g.snomed_id
	)
select *
from sculptor_unfiltered a -- remove correct matches from filter. Correct means that procedure has total amount of relations at most N higher than matches with ICD procedure
where a.rcounter > a.matches + 2 -- == N
;
CREATE INDEX idx_sculptor ON sculptor (procedure_id, snomed_id)
;
analyze sculptor
;
create unlogged table test_group_cleaned as
select t.*
from test_group t
left join sculptor a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where a.procedure_id is null
;
drop table if exists test_group cascade
;
alter table test_group_cleaned rename to test_group
;
drop table if exists sculptor
;
CREATE INDEX idx_test_group	ON test_group (procedure_id, snomed_id, match_on) 
;
analyze test_group;
;*/
--procedures that specify abnormal morphology (except for 40599742 Lesion) cant be present in ICD10PCS
create unlogged table no_morph_allowed as
select t.*
from test_group t
left join concept_relationship cr on
	t.snomed_id = cr.concept_id_1 and
	cr.relationship_id in ('Has proc morph','Has dir morph','Has asso morph') and
	cr.concept_id_2 != 40599742 and -- generic 'lesion'
	cr.invalid_reason is null
left join icd10mappings i on
	t.procedure_id = i.procedure_id and
	i.concept_class_id = 'Procedure'
left join concept_relationship cr2 on --not same lesion as the procedure attribute has
	cr.concept_id_2 = cr2.concept_id_2 and
	cr2.concept_id_1 = i.attribute_id and
	cr2.invalid_reason is null
where cr.concept_id_1 is null or cr2.concept_id_1 is not null
;
analyze no_morph_allowed
;
drop table if exists test_group cascade
;
alter table no_morph_allowed rename to test_group
;
CREATE INDEX idx_test_group ON test_group (procedure_id ASC, snomed_id ASC, match_on asc) 
;
analyze test_group
;
-- sometimes procedures in snomed have multiple related attributes. We only need to keep unique matches for SNOMED attributes
-- A procedure may have for example both 'Upper limb structure' AND 'Vascular structure of forearm', with ICD10PCS procedure having only the second analogue.
-- to find superfluos attributes we rely on concept_ancestor table; match attribute is considered superfluous if the same match is also made with that attributes descendant;
-- therefore, in any chain of ancestorship only the most specific attribute will be kept
create unlogged table incorrect_attributes as
select distinct -- natching pair we check
	ca.procedure_id,
	ca.snomed_id,
	ca.match_on
from test_group ca
join concept_ancestor c on
	c.ancestor_concept_id = ca.match_on and -- all children of the attribute
	c.min_levels_of_separation > 0 -- not the same attribute
join test_group x on
	ca.procedure_id = x.procedure_id and -- exact matching pair is formed with children
	ca.snomed_id = x.snomed_id and
	c.descendant_concept_id = x.match_on
;
CREATE INDEX idx_incorrect_attributes
	ON incorrect_attributes (procedure_id ASC, snomed_id ASC, match_on asc) 
;
analyze incorrect_attributes;
;
create unlogged table test_group1 as
select t.*
from test_group t
left join incorrect_attributes a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id and
	t.match_on = a.match_on
where a.procedure_id is null
;
drop table if exists incorrect_attributes cascade
;
drop table if exists test_group cascade
;
alter table test_group1 rename to test_group
;
CREATE INDEX idx_test_group
	ON test_group (procedure_id ASC, snomed_id ASC) 
;
analyze test_group
;
/*
create unlogged table body_precis as --wrong only
select
	t.procedure_id,
	t.snomed_id
from test_group t

join icd10mappings i on --icd body structure (if applicable)
	i.concept_class_id = 'Body Structure' and
	t.procedure_id = i.procedure_id and
	not --bypass and transposition are allowed to have multiple body structures
		(
			substr (i.procedure_code,1,1) = '0' and
			substr (i.procedure_code,3,1) in ('1','X')
		)
	
join concept_relationship cr on --snomed body structure (if applicable)
	t.snomed_id = cr.concept_id_1 and
	cr.invalid_reason is null
join relations rr on
	rr.relationship_id = cr.relationship_id
join concept c2 on 
	c2.concept_class_id = 'Body Structure' and
	cr.concept_id_2 = c2.concept_id
left join concept_ancestor ca on --intersection
	c2.concept_id = ca.ancestor_concept_id and
	i.attribute_id = ca.descendant_concept_id
where 
	ca.descendant_concept_id is null */
create unlogged table body_precis as
select
	t.procedure_id,
	t.snomed_id
from test_group	t

join concept_relationship cr on --body structures in SNOMED procedures
	cr.invalid_reason is null and
	t.snomed_id = cr.concept_id_1
join relations r on
	r.relationship_id = cr.relationship_id
join concept c2 on
	c2.concept_id = cr.concept_id_2 and
	c2.vocabulary_id = 'SNOMED' and
	c2.concept_class_id = 'Body Structure'

where not exists
	(
		select 1
		from concept_ancestor ca
		join icd10mappings i on --body structures in ICD10 procedures
			i.procedure_id = t.procedure_id and
			i.concept_class_id = 'Body Structure' and
			ca.descendant_concept_id = c2.concept_id
	)
;
CREATE INDEX idx_body_precis ON body_precis (procedure_id, snomed_id) 
;
analyze body_precis
;
create unlogged table test_group1 as
select t.*
from test_group t
left join body_precis a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where a.procedure_id is null
;
drop table if exists body_precis cascade
;
drop table if exists test_group cascade
;
alter table test_group1 rename to test_group
;
CREATE INDEX idx_test_group
	ON test_group (snomed_id asc, procedure_id asc) 
;
analyze test_group;
;
--SNOMED 'replacements' are both 'removals' and 'placements', so we ensure that no separate 'insertion' or 'removal' in ICD10PCS will get coded as 'replacement'
create unlogged table removal_id as--for removals
	(
		select i.procedure_id
		from icd10mappings i
		where
			(--removal surgical; code pattern 0.P....
				substr (i.procedure_code,1,1) = '0' and
				substr (i.procedure_code,3,1) = 'P'
			)
			or
			(--removal of device; code pattern 2.5....
				substr (i.procedure_code,1,1) = '2' and
				substr (i.procedure_code,3,1) = '5'
			)
			or
			(--Repair; code pattern 0.Q.... -- grafts/implants belong in other chapter
				substr (i.procedure_code,1,1) = '0' and
				substr (i.procedure_code,3,1) = 'Q'
			)
			or
			substr (i.procedure_code,1,1) = 'B' --whole imaging chapter. eliminates whole angiographic stent insertion stuff while we are at it
	)
;
delete from test_group t
where
	exists (select procedure_id from removal_id) and
	snomed_id in (select descendant_concept_id from concept_ancestor where ancestor_concept_id in (4027403,4132647)) --introduction, construction
;
drop table if exists removal_id cascade
;
create unlogged table insertion_id as -- for insertions
	(
		select i.procedure_id
		from icd10mappings i
		where
			(--insertion surgical; pattern 0.H.... or 1.H....
				substr (i.procedure_code,1,1) in ('0','1') and
				substr (i.procedure_code,3,1) = 'H'
			)
	)
;
delete from test_group
where
	procedure_id in (select * from insertion_id) and
	snomed_id in (select descendant_concept_id from concept_ancestor where ancestor_concept_id in (4042150,4300185,4000731)) --removal, transplantation, grafting
;
drop table if exists insertion_id cascade
;
create unlogged table radio_id as -- delete brachytherapy from surgical destruction -- has its own chapter
	(
		select i.procedure_id
		from icd10mappings i
		where
			(--destruction surgical; pattern 0.5....
				substr (i.procedure_code,1,1) = '0' and
				substr (i.procedure_code,3,1) = '5'
			)
	)
;
delete from test_group
where
	procedure_id in (select * from radio_id) and
	snomed_id in (select descendant_concept_id from concept_ancestor where ancestor_concept_id in (4029715,4180941)) --radiotherapy, procedures with specified devices
;
delete from test_group where
	exists (select procedure_id from radio_id) and
	snomed_id in (select concept_id_1 from concept_relationship where concept_id_2 = 4133890 and invalid_reason is null) --stereotactic surgery belongs in other chapter
;
drop table if exists radio_id cascade
;
--4
create unlogged table test_group4 as select * from test_group
;
analyze test_group4
;
create unlogged table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
with anat_temp as
	(
		select
			procedure_id,
			snomed_id,
			ac,
			min (ac) over (partition by procedure_id) as min_ac
		from test_group4
		where priority = 4 -- usually corresponds to Body Structure
	)
select procedure_id, snomed_id
from anat_temp a
where a.ac > a.min_ac;

insert into anatomical -- mappings to snomed procedures that dont have the tested attribute at all, while other mapping options do
with anatomical_procedures as --which icd10 procedures have options with said attribute at all
	(
		select procedure_id
		from test_group4
		where priority = 4
	),
good_proced as
	(
		select
			procedure_id,
			snomed_id
		from test_group4
		where priority = 4
	),
distillate as
	(
		select
			t.procedure_id,
			t.snomed_id,
			t.priority
		from test_group4 t
		join anatomical_procedures a on
					t.procedure_id = a.procedure_id
	)
select
		t.procedure_id,
		t.snomed_id
from distillate t
--testing sequence
left join good_proced g on
		t.procedure_id = g.procedure_id and
		t.snomed_id = g.snomed_id
where g.procedure_id is null
;
commit
;
create unlogged table test_group4_fixed as
select t.*
from test_group4 t
left join anatomical a on
		t.procedure_id = a.procedure_id and
		t.snomed_id = a.snomed_id
where
		a.procedure_id is NULL
;
drop table if exists test_group4 cascade
;
alter table test_group4_fixed rename to test_group4
;

CREATE INDEX idx_test_group4
	ON test_group4 (procedure_id, snomed_id, match_on) 
;
analyze test_group4;
;
drop table if exists anatomical cascade
;
drop table if exists maxfix cascade --we keep only matches with the highest number of matched attributes
;
create unlogged table maxfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into maxfix
	with x as
		(
			select distinct  -- count number of matched attributes for every match pair
				procedure_id,
				snomed_id,
				count (match_on) as c
			from test_group4
			group by procedure_id,snomed_id
		),
	maxim as
		(
			select distinct procedure_id, max(c) as m  --define highest number for every ICD10PCS procedure
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x
	join maxim ma on
		ma.m > x.c and
		ma.procedure_id = x.procedure_id
;
drop table if exists group_mid cascade
;
create unlogged table group_mid as --remove those with less matches
	(
		select t.* from test_group4 t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create INDEX idx_group_mid ON group_mid (procedure_id, snomed_id, ac) 
;
analyze group_mid;
;
commit
;
drop table if exists minfix cascade --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
;
create unlogged table minfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into minfix
	with x as
		(
			select  -- find sum of distances for every match pair
				procedure_id,
				snomed_id,
				sum (ac)
					over (partition by procedure_id, snomed_id) as c
			from group_mid
		),
	minim as --find lowest possible sum for every ICD10PCS procedure
		(
			select distinct procedure_id, min(c) as m
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
	join minim mi on
		mi.m < x.c and
		mi.procedure_id = x.procedure_id
;
drop table if exists group_finalised cascade
;
create unlogged table group_finalised as -- recreate unlogged table without pairs with higher sums
	(
		select distinct
			t.procedure_id,
			t.snomed_id,
			t.depth,
			4 as pgroup,
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create unlogged table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
		(
			select distinct procedure_id, min(depth) as m
			from group_finalised
			group by procedure_id
		)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should not be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end,
	g.pgroup
from group_finalised g
join mindeep m on
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table if exists mindeep cascade
;
drop table if exists test_group4 cascade
;
--5
create unlogged table test_group5 as select * from test_group
;
CREATE INDEX idx_test_group5 ON test_group5 (procedure_id, snomed_id, ac) 
;
analyze test_group5;
;
create unlogged table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
with anat_temp as
		(
		 select
	procedure_id,
	snomed_id,
	ac,
	min (ac) over (partition by procedure_id) as min_ac
		 from test_group5
		 where priority = 5 -- usually corresponds to Body Structure
		)
select procedure_id, snomed_id
from anat_temp a
where a.ac > a.min_ac;

insert into anatomical -- mappings to snomed procedures that dont have the tested attribute at all, while other mapping options do
with anatomical_procedures as --which icd10 procedures have options with said attribute at all
	(
		select procedure_id
		from test_group5
		where priority = 5
	),
good_proced as
	(
		select
			procedure_id,
			snomed_id
		from test_group5
		where priority = 5
	),
distillate as
	(
		select
			t.procedure_id,
			t.snomed_id,
			t.priority
		from test_group5 t
		join anatomical_procedures a on
					t.procedure_id = a.procedure_id
	)
select
		t.procedure_id,
		t.snomed_id
from distillate t
--testing sequence
left join good_proced g on
		t.procedure_id = g.procedure_id and
		t.snomed_id = g.snomed_id
where g.procedure_id is null
;
commit
;
create unlogged table test_group5_fixed as
select t.*
from test_group5 t
left join anatomical a on
		t.procedure_id = a.procedure_id and
		t.snomed_id = a.snomed_id
where
		a.procedure_id is NULL
;
drop table if exists test_group5 cascade
;
alter table test_group5_fixed rename to test_group5
;
CREATE INDEX idx_test_group5 ON test_group5 (procedure_id, snomed_id, match_on) 
;
analyze test_group5
;
drop table if exists anatomical cascade
;
drop table if exists maxfix cascade --we keep only matches with the highest number of matched attributes
;
create unlogged table maxfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into maxfix
	with x as
		(
			select distinct  -- count number of matched attributes for every match pair
				procedure_id,
				snomed_id,
				count (match_on) as c
			from test_group5
			group by procedure_id,snomed_id
		),
	maxim as
		(
			select distinct procedure_id, max(c) as m  --define highest number for every ICD10PCS procedure
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x
	join maxim ma on
		ma.m > x.c and
		ma.procedure_id = x.procedure_id
;
drop table if exists group_mid cascade
;
create unlogged table group_mid as --remove those with less matches
	(
		select t.* from test_group5 t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create INDEX idx_group_mid ON group_mid (procedure_id, snomed_id, ac)
;
analyze group_mid
;
commit
;
drop table if exists minfix cascade --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
;
create unlogged table minfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into minfix
	with x as
		(
			select  -- find sum of distances for every match pair
				procedure_id,
				snomed_id,
				sum (ac)
					over (partition by procedure_id, snomed_id) as c
			from group_mid
		),
	minim as --find lowest possible sum for every ICD10PCS procedure
		(
			select distinct procedure_id, min(c) as m
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
	join minim mi on
		mi.m < x.c and
		mi.procedure_id = x.procedure_id
;
drop table if exists group_finalised cascade
;
create unlogged table group_finalised as -- recreate unlogged table without pairs with higher sums
	(
		select distinct
			t.procedure_id,
			t.snomed_id,
			t.depth,
			5 as pgroup,
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create unlogged table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
		(
			select distinct procedure_id, min(depth) as m
			from group_finalised
			group by procedure_id
		)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should not be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end,
	g.pgroup
from group_finalised g
join mindeep m on
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table if exists mindeep cascade
;
drop table if exists test_group5 cascade
;
--7
create unlogged table test_group7 as
select t.* from test_group t
join concept c on
	t.procedure_id = c.concept_id and
	substr (c.concept_code,7,1) != 'Z' -- No 'No Qualifier'
;
CREATE INDEX idx_test_group7 ON test_group7 (procedure_id, snomed_id, ac) 
;
analyze test_group7;
;
create unlogged table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
with anat_temp as
	(
		select
			procedure_id,
			snomed_id,
			ac,
			min (ac) over (partition by procedure_id) as min_ac
		from test_group7
		where priority = 7 -- usually corresponds to Body Structure
	)
select procedure_id, snomed_id
from anat_temp a
where a.ac > a.min_ac
;
insert into anatomical -- mappings to snomed procedures that dont have the tested attribute at all, while other mapping options do
with anatomical_procedures as --which icd10 procedures have options with said attribute at all
		(
		 select procedure_id
		 from test_group7
		 where priority = 7
		),
good_proced as
	(
		select
			procedure_id,
			snomed_id
		from test_group7
		where priority = 7
	),
distillate as
	(
		select
			t.procedure_id,
			t.snomed_id,
			t.priority
		from test_group7 t
		join anatomical_procedures a on
					t.procedure_id = a.procedure_id
	)
select
		t.procedure_id,
		t.snomed_id
from distillate t
--testing sequence
left join good_proced g on
		t.procedure_id = g.procedure_id and
		t.snomed_id = g.snomed_id
where g.procedure_id is null
;
commit
;
create unlogged table test_group7_fixed as
select t.*
from test_group7 t
left join anatomical a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where
	a.procedure_id is NULL
;
drop table if exists test_group7 cascade
;
alter table test_group7_fixed rename to test_group7
;
CREATE INDEX idx_test_group7
	ON test_group7 (procedure_id, snomed_id, match_on) 
;
analyze test_group7
;
drop table if exists anatomical cascade
;
drop table if exists maxfix cascade --we keep only matches with the highest number of matched attributes
;
create unlogged table maxfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into maxfix
with x as
	(
		select distinct -- count number of matched attributes for every match pair
			procedure_id,
			snomed_id,
			count (match_on) as c
		from test_group7
		group by procedure_id,snomed_id
	),
maxim as
	(
		select distinct procedure_id, max(c) as m --define highest number for every ICD10PCS procedure
		from x
		group by procedure_id
	)
select x.procedure_id,x.snomed_id from x
join maxim ma on
	ma.m > x.c and
	ma.procedure_id = x.procedure_id
;
drop table if exists group_mid cascade
;
create unlogged table group_mid as --remove those with less matches
	(
		select t.* from test_group7 t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create INDEX idx_group_mid ON group_mid (procedure_id, snomed_id, ac) 
;
analyze group_mid;
;
commit
;
drop table if exists minfix cascade --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
;
create unlogged table minfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into minfix
	with x as
		(
			select -- find sum of distances for every match pair
				procedure_id,
				snomed_id,
				sum (ac)
					over (partition by procedure_id, snomed_id) as c
			from group_mid
		),
	minim as --find lowest possible sum for every ICD10PCS procedure
		(
			select distinct procedure_id, min(c) as m
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
	join minim mi on
		mi.m < x.c and
		mi.procedure_id = x.procedure_id
;
drop table if exists group_finalised cascade
;
create unlogged table group_finalised as -- recreate unlogged table without pairs with higher sums
	(
		select distinct
			t.procedure_id,
			t.snomed_id,
			t.depth,
			7 as pgroup,
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create unlogged table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
		(
			select distinct procedure_id, min(depth) as m
			from group_finalised
			group by procedure_id
		)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should not be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end,
	g.pgroup
from group_finalised g
join mindeep m on
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table if exists mindeep cascade
;
drop table if exists test_group7 cascade
;
CREATE INDEX idx_MAPPINGS ON MAPPINGS (PROCEDURE_ID, SNOMED_ID, PRIORITY) 
;
analyze MAPPINGS;
;
drop table if exists test_group cascade
;
drop table if exists method_whitelist cascade
;
create table method_whitelist as
-- create acceptable 'Has method' list for each procedure using mappings
-- include: methods and their ancestors AND methods as attributes and their ancestors
select distinct 
	i.procedure_id,
	ca.ancestor_concept_id as method_id
from icd10mappings i
join concept_relationship cr on-- procedures -> methods
	cr.relationship_id in ('Has method','Has revision status') and
	cr.concept_id_1 = i.attribute_id and
	i.concept_class_id = 'Procedure' and
	cr.invalid_reason is null
join concept_ancestor ca on --methods -> parents
	ca.descendant_concept_id = concept_id_2
;
insert into method_whitelist
select distinct
	i.procedure_id,
	ca.ancestor_concept_id as method_id
from icd10mappings i
join concept_ancestor ca on --methods -> parents
	ca.descendant_concept_id = i.attribute_id and
	i.concept_class_id = 'Qualifier Value'
;
insert into method_whitelist
select distinct --surgery
	i.procedure_id,
	4045049 as method_id
from icd10mappings i
;
create index idx_mw on method_whitelist (procedure_id, method_id)
;
analyze method_whitelist
;
-- Now we do the same, but with Procedure parent instead of procedure-attributes
-- This way we try to preserve attributes not included in previous iteration, having ICD10PCS procedures being children to possibly generic "procedure referring to attribute"
create unlogged table test_group as	
select distinct -- parents <3
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, lower number = more precise
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	i.concept_class_id, -- to store if attribute is a device; important to determine travelling direction in hierarchy
	i.priority -- number to store corresponding attribute letter position in ICD10PCS code for later filtering
from icd10mappings i
join icd10mappings x on --place horizontally procedures and all other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --include ancestors of all other attributes
	ra.descendant_concept_id = i.attribute_id and
	i.concept_class_id != 'Procedure' and
	ra.max_levels_of_separation <=3 --limit set because of acceptability and RAM limits reasons
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join devv5.concept_ancestor a on --Get all SP procedure descendants
	r.concept_id_1 = a.descendant_concept_id and
	a.ancestor_concept_id = 4322976 -- Procedure
--only include generic procedures without specified METHOD (except for general 'Surgery') or specified methods in whitelist
left join concept_relationship cf on
	cf.concept_id_1 = a.descendant_concept_id and
	cf.relationship_id in ('Has method','Has revision status') and
	cf.invalid_reason is null
left join method_whitelist w on
	cf.concept_id_2 = w.method_id and
	w.procedure_id = i.procedure_id
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on
	 s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on
	 s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join sources.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
where (cf.concept_id_1 is null) = (w.procedure_id is null) -- either both are null or neither is
;
insert into test_group
select distinct -- parents 4-6
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, lower number = more precise
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	i.concept_class_id, -- to store if attribute is a device; important to determine travelling direction in hierarchy
	i.priority -- number to store corresponding attribute letter position in ICD10PCS code for later filtering
from icd10mappings i
join icd10mappings x on --place horizontally procedures and all other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --include ancestors of all other attributes
	ra.descendant_concept_id = i.attribute_id and
	i.concept_class_id != 'Procedure' and
	ra.max_levels_of_separation <=6 and --limit set because of acceptability and RAM limits reasons
	ra.max_levels_of_separation > 3
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join devv5.concept_ancestor a on --Get all SP procedure descendants
	r.concept_id_1 = a.descendant_concept_id and
	a.ancestor_concept_id = 4322976 -- Procedure
--only include generic procedures without specified METHOD (except for general 'Surgery') or specified methods in whitelist
left join concept_relationship cf on
	cf.concept_id_1 = a.descendant_concept_id and
	cf.relationship_id in ('Has method','Has revision status') and
	cf.invalid_reason is null
left join method_whitelist w on
	cf.concept_id_2 = w.method_id and
	w.procedure_id = i.procedure_id
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on
	 s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on
	 s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join sources.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
where (cf.concept_id_1 is null) = (w.procedure_id is null) -- either both are null or neither is
;
insert into test_group
select distinct -- parents 7-8
	i.procedure_id, --icd10 proc
	a.descendant_concept_id as snomed_id, --snomed proc match candidate
	ra.ancestor_concept_id as match_on, --matching attribute id
	ra.min_levels_of_separation as ac, --how far up is the mapping of the attribute, lower number = more precise
	a.max_levels_of_separation as depth, --how far down is the mapping of the procedure, less specific is better (avoid adding detalisation)
	i.concept_class_id, -- to store if attribute is a device; important to determine travelling direction in hierarchy
	i.priority -- number to store corresponding attribute letter position in ICD10PCS code for later filtering
from icd10mappings i
join icd10mappings x on --place horizontally procedures and all other attributes
	x.procedure_id = i.procedure_id and
	x.concept_class_id != 'Procedure'
join devv5.concept_ancestor ra on --include ancestors of all other attributes
	ra.descendant_concept_id = i.attribute_id and
	i.concept_class_id != 'Procedure' and
	ra.max_levels_of_separation <=8 and --limit set because of acceptability and RAM limits reasons
	ra.max_levels_of_separation > 6
join concept_relationship r on -- find matching pairs of procedures and attributes in SNOMED
	r.concept_id_2 = ra.ancestor_concept_id and
	r.invalid_reason is null
join devv5.concept_ancestor a on --Get all SP procedure descendants
	r.concept_id_1 = a.descendant_concept_id and
	a.ancestor_concept_id = 4322976 -- Procedure
--only include generic procedures without specified METHOD (except for general 'Surgery') or specified methods in whitelist
left join concept_relationship cf on
	cf.concept_id_1 = a.descendant_concept_id and
	cf.relationship_id in ('Has method','Has revision status') and
	cf.invalid_reason is null
left join method_whitelist w on
	cf.concept_id_2 = w.method_id and
	w.procedure_id = i.procedure_id
join relations e on --ensure validness of relationship; excludes other vocabularies and speeds up search
	e.relationship_id = r.relationship_id
join concept s on
	 s.concept_id=r.concept_id_1 and s.vocabulary_id = 'SNOMED'
join concept s1 on
	 s1.concept_id=r.concept_id_2 and s1.vocabulary_id = 'SNOMED'
join sources.mrconso u on --filter non-international SNOMED, lower amount of possible concepts but much higher quality of relation between procedures and attributes
	s.concept_code = CODE and u.sab = 'SNOMEDCT_US'
where (cf.concept_id_1 is null) = (w.procedure_id is null) -- either both are null or neither is
;
CREATE INDEX idx_test_group ON test_group (procedure_id, snomed_id, match_on)
;
analyze test_group;
;/*
-- remove procedures with too many excessive attributes
create unlogged table sculptor as
with true_counts as
	(
		select --how many relations to attributes each SNOMED procedure has
			cr.concept_id_1 as snomed_id,
			count (cr.concept_id_2) as rcounter
		from concept_relationship cr
		join relations r on --limit to valid attribute relations
			r.relationship_id = cr.relationship_id
		join concept c on --search among snomed procedures
			c.vocabulary_id = 'SNOMED' and
			c.standard_concept = 'S' and
			c.concept_class_id = 'Procedure' and
			cr.concept_id_1 = c.concept_id
		join concept c2 on --only snomed attributes
			concept_id_2 = c2.concept_id and
			c2.vocabulary_id = 'SNOMED'
		group by cr.concept_id_1
	),
sculptor_unfiltered as
	(
		select distinct
			g.procedure_id,
			t.rcounter,
			t.snomed_id,
			count (g.match_on) over (partition by g.procedure_id, g.snomed_id) as matches
		from test_group g
		join true_counts t on
			t.snomed_id = g.snomed_id
	)
select *
from sculptor_unfiltered a -- remove correct matches from filter. Correct means that procedure has total amount of relations at most N higher than matches with ICD procedure
where a.rcounter > a.matches + 2 -- == N
;
CREATE INDEX idx_sculptor ON sculptor (procedure_id, snomed_id, matches) 
;
analyze sculptor
;
create unlogged table test_group_cleaned as
select t.*
from test_group t
left join sculptor a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where a.procedure_id is null
;
drop table if exists test_group cascade
;
alter table test_group_cleaned rename to test_group
;
drop table if exists sculptor cascade
;
CREATE INDEX idx_test_group	ON test_group (procedure_id, snomed_id) 
;
analyze test_group
;*/
;
/*
create unlogged table body_precis as --wrong only
select
	t.procedure_id,
	t.snomed_id
from test_group t

join icd10mappings i on --icd body structure (if applicable)
	i.concept_class_id = 'Body Structure' and
	t.procedure_id = i.procedure_id and
	not --bypass and transposition are allowed to have multiple body structures
		(
			substr (i.procedure_code,1,1) = '0' and
			substr (i.procedure_code,3,1) in ('1','X')
		)
	
join concept_relationship cr on --snomed body structure (if applicable)
	t.snomed_id = cr.concept_id_1 and
	cr.invalid_reason is null
join relations rr on
	rr.relationship_id = cr.relationship_id
join concept c2 on 
	c2.concept_class_id = 'Body Structure' and
	cr.concept_id_2 = c2.concept_id
left join concept_ancestor ca on --intersection
	c2.concept_id = ca.ancestor_concept_id and
	i.attribute_id = ca.descendant_concept_id
where 
	ca.descendant_concept_id is null */
create unlogged table body_precis as
select
	t.procedure_id,
	t.snomed_id
from test_group	t

join concept_relationship cr on --body structures in SNOMED procedures
	cr.invalid_reason is null and
	t.snomed_id = cr.concept_id_1
join relations r on
	r.relationship_id = cr.relationship_id
join concept c2 on
	c2.concept_id = cr.concept_id_2 and
	c2.vocabulary_id = 'SNOMED' and
	c2.concept_class_id = 'Body Structure'

where not exists
	(
		select 1
		from concept_ancestor ca
		join icd10mappings i on --body structures in ICD10 procedures
			i.procedure_id = t.procedure_id and
			i.concept_class_id = 'Body Structure' and
			ca.descendant_concept_id = c2.concept_id
	)
;
CREATE INDEX idx_body_precis ON body_precis (procedure_id, snomed_id) 
;
analyze body_precis
;
create unlogged table test_group1 as
select t.*
from test_group t
left join body_precis a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where a.procedure_id is null
;
drop table if exists body_precis cascade
;
drop table if exists test_group cascade
;
alter table test_group1 rename to test_group
;
CREATE INDEX idx_test_group ON test_group (snomed_id, procedure_id) 
;
analyze test_group
;
--procedures that specify abnormal morphology (except for 40599742 Lesion) cant be present in ICD10PCS
create unlogged table no_morph_allowed as
select t.*
from test_group t
left join concept_relationship cr on
	t.snomed_id = cr.concept_id_1 and
	cr.relationship_id in ('Has proc morph','Has dir morph','Has asso morph') and
	cr.concept_id_2 != 40599742 and -- generic 'lesion'
	cr.invalid_reason is null
left join icd10mappings i on
	t.procedure_id = i.procedure_id and
	i.concept_class_id = 'Procedure'
left join concept_relationship cr2 on --not same lesion as the procedure attribute has
	cr.concept_id_2 = cr2.concept_id_2 and
	cr2.concept_id_1 = i.attribute_id and
	cr2.invalid_reason is null
where cr.concept_id_1 is null or cr2.concept_id_1 is not null
;
analyze no_morph_allowed
;
drop table if exists test_group cascade
;
alter table no_morph_allowed rename to test_group
;
CREATE INDEX idx_test_group ON test_group (procedure_id ASC, snomed_id ASC, match_on asc) 
;
analyze test_group
;
-- sometimes procedures in snomed have multiple related attributes. We only need to keep unique matches for SNOMED attributes
-- A procedure may have for example both 'Upper limb structure' AND 'Vascular structure of forearm', with ICD10PCS procedure having only the second analogue.
-- to find superfluos attributes we rely on concept_ancestor table; match attribute is considered superfluous if the same match is also made with that attributes descendant;
-- therefore, in any chain of ancestorship only the most specific attribute will be kept
create unlogged table incorrect_attributes as
select distinct -- natching pair we check
	ca.procedure_id,
	ca.snomed_id,
	ca.match_on
from test_group ca
join concept_ancestor c on
	c.ancestor_concept_id = ca.match_on and -- all children of the attribute
	c.min_levels_of_separation > 0 -- not the same attribute
join test_group x on
	ca.procedure_id = x.procedure_id and -- exact matching pair is formed with children
	ca.snomed_id = x.snomed_id and
	c.descendant_concept_id = x.match_on
;
CREATE INDEX idx_incorrect_attributes
	ON incorrect_attributes (procedure_id, snomed_id, match_on) 
;
analyze incorrect_attributes;
;
create unlogged table test_group1 as
select t.*
from test_group t
left join incorrect_attributes a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id and
	t.match_on = a.match_on
where a.procedure_id is null
;
drop table if exists incorrect_attributes cascade
;
drop table if exists test_group cascade
;
alter table test_group1 rename to test_group
;
analyze test_group;
;
--4
create unlogged table test_group4 as select * from test_group
;
create index idx_test_group4 on test_group4 (procedure_id, snomed_id, ac)
;
analyze test_group4
;
create unlogged table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
with anat_temp as
	(
		select
			procedure_id,
			snomed_id,
			ac,
			min (ac) over (partition by procedure_id) as min_ac
		from test_group4
		where priority = 4 -- usually corresponds to Body Structure
	)
select procedure_id, snomed_id
from anat_temp a
where a.ac > a.min_ac
;
create table anatomical_procedures as --which icd10 procedures have options with said attribute at all
select distinct procedure_id
from test_group4
where priority = 4
;
create index idx_ap on anatomical_procedures (procedure_id)
;
analyze anatomical_procedures
;
create table good_proced as
	(
		select
			t4.procedure_id,
			t4.snomed_id
		from test_group4 t4
--		join anatomical_procedures ap on
--			t4.procedure_id = ap.procedure_id
		where priority = 4
	)
;
create index idx_gp on good_proced (procedure_id,snomed_id)
;
analyze good_proced
;
insert into anatomical -- mappings to snomed procedures that dont have the tested attribute at all, while other mapping options do
with distillate as
	(
		select
			t.procedure_id,
			t.snomed_id,
			t.priority
		from test_group4 t
		join anatomical_procedures a on
					t.procedure_id = a.procedure_id
	)
select
	t.procedure_id,
	t.snomed_id
from distillate t
--testing sequence
left join good_proced g on
	t.procedure_id = g.procedure_id and
	t.snomed_id = g.snomed_id
where g.procedure_id is null
;
drop table if exists anatomical_procedures cascade
;
drop table if exists good_proced cascade
;
create index idx_anatomical on anatomical (procedure_id, snomed_id)
;
analyze anatomical
;
create unlogged table test_group4_fixed as
select t.*
from test_group4 t
left join anatomical a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where
	a.procedure_id is NULL
;
drop table if exists test_group4 cascade
;
alter table test_group4_fixed rename to test_group4
;
CREATE INDEX idx_test_group4 ON test_group4 (procedure_id, snomed_id, match_on) 
;
analyze test_group4
;
drop table if exists anatomical cascade
;
drop table if exists maxfix cascade --we keep only matches with the highest number of matched attributes
;
create unlogged table maxfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into maxfix
with x as
	(
		select distinct -- count number of matched attributes for every match pair
			procedure_id,
			snomed_id,
			count (match_on) as c
		from test_group4
		group by procedure_id,snomed_id
	),
maxim as
	(
		select distinct procedure_id, max(c) as m --define highest number for every ICD10PCS procedure
		from x
		group by procedure_id
	)
select x.procedure_id,x.snomed_id from x
join maxim ma on
	ma.m > x.c and
	ma.procedure_id = x.procedure_id
;
drop table if exists group_mid cascade
;
create unlogged table group_mid as --remove those with less matches
	(
		select t.* from test_group4 t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create INDEX idx_group_mid ON group_mid (procedure_id, snomed_id, ac) 
;
analyze group_mid;
;
commit
;
drop table if exists minfix cascade --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
;
create unlogged table minfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into minfix
	with x as
		(
			select -- find sum of distances for every match pair
				procedure_id,
				snomed_id,
				sum (ac)
					over (partition by procedure_id, snomed_id) as c
			from group_mid
		),
	minim as --find lowest possible sum for every ICD10PCS procedure
		(
			select distinct procedure_id, min(c) as m
			from x
			group by procedure_id
		)
	select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
	join minim mi on
		mi.m < x.c and
		mi.procedure_id = x.procedure_id
;
drop table if exists group_finalised cascade
;
create unlogged table group_finalised as -- recreate unlogged table without pairs with higher sums
	(
		select distinct
			t.procedure_id,
			t.snomed_id,
			t.depth,
			14 as pgroup,
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create unlogged table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
		(
			select distinct procedure_id, min(depth) as m
			from group_finalised
			group by procedure_id
		)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should not be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end,
	g.pgroup
from group_finalised g
join mindeep m on
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table if exists mindeep cascade
;
drop table if exists test_group4 cascade
;
--5
create unlogged table test_group5 as select * from test_group;
;
create index idx_test_group5 on test_group5 (procedure_id, snomed_id, ac)
;
analyze test_group5
;
drop table if exists anatomical cascade
;
create unlogged table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
with anat_temp as
	(
		select
			procedure_id,
			snomed_id,
			ac,
			min (ac) over (partition by procedure_id) as min_ac
		from test_group5
		where priority = 5 -- usually corresponds to Body Structure
	)
select procedure_id, snomed_id
from anat_temp a
where a.ac > a.min_ac
;
create table anatomical_procedures as --which icd10 procedures have options with said attribute at all
select distinct procedure_id
from test_group5
where priority = 5
;
create index idx_ap on anatomical_procedures (procedure_id)
;
analyze anatomical_procedures
;
create table good_proced as
	(
		select
			t5.procedure_id,
			t5.snomed_id
		from test_group5 t5
--		join anatomical_procedures ap on
--			t5.procedure_id = ap.procedure_id
		where priority = 5
	)
;
create index idx_gp on good_proced (procedure_id,snomed_id)
;
analyze good_proced
;
insert into anatomical -- mappings to snomed procedures that dont have the tested attribute at all, while other mapping options do
with distillate as
	(
		select
			t.procedure_id,
			t.snomed_id,
			t.priority
		from test_group5 t
		join anatomical_procedures a on
					t.procedure_id = a.procedure_id
	)
select
	t.procedure_id,
	t.snomed_id
from distillate t
--testing sequence
left join good_proced g on
	t.procedure_id = g.procedure_id and
	t.snomed_id = g.snomed_id
where g.procedure_id is null
;
drop table if exists anatomical_procedures cascade
;
drop table if exists good_proced cascade
;
create index idx_anatomical on anatomical (procedure_id, snomed_id)
;
analyze anatomical
;
create unlogged table test_group5_fixed as
select t.*
from test_group5 t
left join anatomical a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where
	a.procedure_id is NULL
;
drop table if exists test_group5 cascade
;
alter table test_group5_fixed rename to test_group5
;
CREATE INDEX idx_test_group5 ON test_group5 (procedure_id, snomed_id, match_on) 
;
analyze test_group5
;
drop table if exists anatomical cascade
;
drop table if exists maxfix cascade --we keep only matches with the highest number of matched attributes
;
create unlogged table maxfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into maxfix
with x as
	(
		select distinct -- count number of matched attributes for every match pair
			procedure_id,
			snomed_id,
			count (match_on) as c
		from test_group5
		group by procedure_id,snomed_id
	),
maxim as
	(
		select distinct procedure_id, max(c) as m --define highest number for every ICD10PCS procedure
		from x
		group by procedure_id
	)
select x.procedure_id,x.snomed_id from x
	join maxim ma on
		ma.m > x.c and
		ma.procedure_id = x.procedure_id
;
drop table if exists group_mid cascade
;
create unlogged table group_mid as --remove those with less matches
	(
		select t.* from test_group5 t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create INDEX idx_group_mid
	ON group_mid (procedure_id, snomed_id, ac) 
;
analyze group_mid;
;
commit
;
drop table if exists minfix cascade --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
;
create unlogged table minfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into minfix
with x as
	(
		select -- find sum of distances for every match pair
			procedure_id,
			snomed_id,
			sum (ac)
				over (partition by procedure_id, snomed_id) as c
		from group_mid
	),
minim as --find lowest possible sum for every ICD10PCS procedure
	(
		select distinct procedure_id, min(c) as m
		from x
		group by procedure_id
	)
select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
join minim mi on
	mi.m < x.c and
	mi.procedure_id = x.procedure_id
;
drop table if exists group_finalised cascade
;
create unlogged table group_finalised as -- recreate unlogged table without pairs with higher sums
	(
		select distinct
			t.procedure_id,
			t.snomed_id,
			t.depth,
			15 as pgroup,
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create unlogged table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
	(
		select distinct procedure_id, min(depth) as m
		from group_finalised
		group by procedure_id
	)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should NOT be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end,
	g.pgroup
from group_finalised g
join mindeep m on
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table if exists mindeep cascade
;
drop table if exists test_group5
;
--7
create unlogged table test_group7 as
select t.* from test_group t
join concept c on
	t.procedure_id = c.concept_id and
	substr (c.concept_code,7,1) != 'Z' -- No 'No Qualifier'
;
create index idx_test_group7 on test_group7 (procedure_id, snomed_id, ac)
;
analyze test_group7
;
create unlogged table anatomical as --does procedure relate to anatomical site in any of its mappings? if yes, all match candidates with no relation to body structure should be dropped
with anat_temp as
	(
		select
			procedure_id,
			snomed_id,
			ac,
			min (ac) over (partition by procedure_id) as min_ac
		from test_group7
		where priority = 7 -- usually corresponds to Body Structure
	)
select procedure_id, snomed_id
from anat_temp a
where a.ac > a.min_ac
;
create table anatomical_procedures as --which icd10 procedures have options with said attribute at all
select distinct procedure_id
from test_group7
where priority = 7
;
create index idx_ap on anatomical_procedures (procedure_id)
;
analyze anatomical_procedures
;
create table good_proced as
	(
		select
			t7.procedure_id,
			t7.snomed_id
		from test_group7 t7
--		join anatomical_procedures ap on
--			t7.procedure_id = ap.procedure_id
		where priority = 7
	)
;
create index idx_gp on good_proced (procedure_id,snomed_id)
;
analyze good_proced
;
insert into anatomical -- mappings to snomed procedures that dont have the tested attribute at all, while other mapping options do
with distillate as
	(
		select
			t.procedure_id,
			t.snomed_id,
			t.priority
		from test_group7 t
		join anatomical_procedures a on
					t.procedure_id = a.procedure_id
	)
select
	t.procedure_id,
	t.snomed_id
from distillate t
--testing sequence
left join good_proced g on
	t.procedure_id = g.procedure_id and
	t.snomed_id = g.snomed_id
where g.procedure_id is null
;
drop table if exists anatomical_procedures cascade
;
drop table if exists good_proced cascade
;
create index idx_anatomical on anatomical (procedure_id, snomed_id)
;
analyze anatomical
;
create unlogged table test_group7_fixed as
select t.*
from test_group7 t
left join anatomical a on
	t.procedure_id = a.procedure_id and
	t.snomed_id = a.snomed_id
where
	a.procedure_id is NULL
;
drop table if exists test_group7 cascade
;
alter table test_group7_fixed rename to test_group7
;
CREATE INDEX idx_test_group7 ON test_group7 (procedure_id, snomed_id, match_on) 
;
analyze test_group7
;
drop table if exists anatomical cascade
;
drop table if exists maxfix cascade --we keep only matches with the highest number of matched attributes
;
create unlogged table maxfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into maxfix
with x as
	(
		select distinct -- count number of matched attributes for every match pair
			procedure_id,
			snomed_id,
			count (match_on) as c
		from test_group7
		group by procedure_id,snomed_id
	),
maxim as
	(
		select distinct procedure_id, max(c) as m --define highest number for every ICD10PCS procedure
		from x
		group by procedure_id
	)
select x.procedure_id,x.snomed_id from x
join maxim ma on
	ma.m > x.c and
	ma.procedure_id = x.procedure_id
;
drop table if exists group_mid cascade
;
create unlogged table group_mid as --remove those with less matches
	(
		select t.* from test_group7 t
		left join maxfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create INDEX idx_group_mid ON group_mid (procedure_id, snomed_id, ac) 
;
analyze group_mid
;
commit
;
drop table if exists minfix cascade --attempt to find closest matches by demanding lowest possible sum of distances between all attributes in matching pairs
;
create unlogged table minfix
	(
		procedure_id int4,
		snomed_id int4
	)
;
insert into minfix
with x as
	(
		select -- find sum of distances for every match pair
			procedure_id,
			snomed_id,
			sum (ac)
				over (partition by procedure_id, snomed_id) as c
		from group_mid
	),
minim as --find lowest possible sum for every ICD10PCS procedure
	(
		select distinct procedure_id, min(c) as m
		from x
		group by procedure_id
	)
select x.procedure_id,x.snomed_id from x -- create a list of all pairs that have higher sum
join minim mi on
	mi.m < x.c and
	mi.procedure_id = x.procedure_id
;
drop table if exists group_finalised cascade
;
create unlogged table group_finalised as -- recreate unlogged table without pairs with higher sums
	(
		select distinct
			t.procedure_id,
			t.snomed_id,
			t.depth,
			17 as pgroup,
			sum (ac)
				over (partition by t.procedure_id, t.snomed_id) as missed
		from group_mid t
		left join minfix m on
			m.procedure_id = t.procedure_id and
			m.snomed_id = t.snomed_id
		where m.procedure_id is null
	)
;
create unlogged table mindeep as -- among all remaining matches, keep the most generic procedure (highest ancestor in procedure tree); table stores minimaly possible level of depth among all matches
		(
			select distinct procedure_id, min(depth) as m
			from group_finalised
			group by procedure_id
		)
;
insert into mappings
select distinct
	g.procedure_id,
	g.snomed_id,
	case -- determine mapping precision, propose relatoinship type accordingly -- should not be used at current state
		when missed = 0 then 'Maps to'
		else 'Is a'
	end,
	g.pgroup
from group_finalised g
join mindeep m on
	g.procedure_id = m.procedure_id and
	g.depth = m.m
;
drop table if exists mindeep cascade
;
drop table if exists test_group7 cascade
;
/*
update mappings m
set
	procedure_id = 
	coalesce 
	(
		(select s.procedure_id from splitter s where s.replaced_id = m.procedure_id),
		m.procedure_id
	)
*/
;
--if level 6 is present, add it as a mapping
with full_list as
	(
		select distinct
			icd_id,
			icd_code,
			snomed_code
		from level6
		where snomed_code is not null
			UNION
		select distinct
			icd_id,
			icd_code,
			snomed_code_2
		from level6
		where snomed_code_2 is not null
	)
insert into mappings
select distinct
	coalesce (s.replaced_id, c1.concept_id) as procedure_id,
	c2.concept_id as snomed_id,
	'Is a' as rel_id,
	26 as priority
from full_list f
join concept c1 on
	substr (c1.concept_code,1,6) = f.icd_code and
	c1.concept_class_id = 'ICD10PCS'
left join splitter s on
	s.procedure_id = c1.concept_id
join concept c2 on
	c2.concept_code = f.snomed_code and
	c2.vocabulary_id = 'SNOMED'
;
analyze mappings
;
--removing duplicates/ancestors again
create unlogged table mappings1 as
with min_priority as
	(
		select
			a.procedure_id,
			a.snomed_id,
			min (a.priority) over (partition by a.procedure_id,a.snomed_id) as priority
		from mappings a
		left join splitter s on
			s.replaced_id = a.procedure_id
	)
select m.* from mappings m
join min_priority i on
	i.procedure_id = m.procedure_id and
	i.snomed_id = m.snomed_id and
	i.priority = m.priority
;
drop table if exists mappings cascade
;
alter table mappings1 rename to mappings
;
CREATE INDEX idx_MAPPINGS ON MAPPINGS (PROCEDURE_ID, SNOMED_ID, PRIORITY) 
;
analyze MAPPINGS
;
create table mappings1 as
select 
	coalesce (s.procedure_id,m.procedure_id) as procedure_id,
	m.snomed_id,
	m.rel_id,
	m.priority
from mappings m
left join splitter s on
	s.replaced_id = m.procedure_id
;
drop table if exists mappings cascade
;
alter table mappings1 rename to mappings
;
CREATE INDEX idx_MAPPINGS ON MAPPINGS (PROCEDURE_ID, SNOMED_ID, PRIORITY) 
;
analyze MAPPINGS
;
delete from mappings m
where exists
	(
		select 1
		from mappings a
		join concept_ancestor ca on
			ca.descendant_concept_id = a.snomed_id and
			ca.min_levels_of_separation != 0 and
			m.procedure_id = a.procedure_id and
			m.snomed_id = ca.ancestor_concept_id
	)
;
analyze MAPPINGS
;